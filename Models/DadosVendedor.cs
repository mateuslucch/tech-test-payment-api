using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public abstract class DadosVendedor
    {
        
        public int Id { get; set; }

        [Required,
        StringLength(11, MinimumLength = 11,
                    ErrorMessage = "CPF deve conter 11 dígitos.")]
        [RegularExpression("^[0-9]*$",
                    ErrorMessage = "CPF deve conter apenas números...")]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }

        [Required,
        StringLength(11, MinimumLength = 11,
            ErrorMessage = "Informe um numero de telefone com código de área...")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numero de telefone deve conter apenas numeros...")]
        public string Telefone { get; set; }

        [Required,
        RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "Informe um email válido...")]
        public string Email { get; set; }
    }
}