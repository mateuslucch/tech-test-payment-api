namespace tech_test_payment_api.Entities
{

    /// <summary>
    /// Enum dos possíveis status para a venda
    /// <summary>
    
    public enum EnumStatus
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_Para_Transportadora,
        Entregue,
        Cancelada

    }
}