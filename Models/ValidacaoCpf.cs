namespace tech_test_payment_api.Models
{
    /// <summary>
    /// Classe para testar se um CPF é válido
    /// Fonte de referência: https://dicasdeprogramacao.com.br/algoritmo-para-validar-cpf/    
    /// <summary>

    static class ValidacaoCpf
    {
        public static bool TesteCpf(string cpf)
        {
            int digito1 = int.Parse(cpf.ToCharArray(9, 1));
            int digito2 = int.Parse(cpf.ToCharArray(10, 1));

            bool confirmacaoDigito1 = false;
            bool confirmacaoDigito2 = false;

            int soma1 = 0;
            int soma2 = 0;

            // Teste digito 1
            for (int i = 0; i < 9; i++)
            {
                soma1 += int.Parse(cpf.ToCharArray(i, 1)) * (10 - i);
            }

            confirmacaoDigito1 = ValidacaoDigito(digito1, soma1);

            // Teste digito 2            
            for (int i = 0; i < 10; i++)
            {
                soma2 += int.Parse(cpf.ToCharArray(i, 1)) * (11 - i);
            }

            confirmacaoDigito2 = ValidacaoDigito(digito2, soma2);

            if (confirmacaoDigito1 == true && confirmacaoDigito2 == true)
                return true;

            return false;

        }

        private static bool ValidacaoDigito(int digito, int soma)
        {
            int resto = soma % 11;
            if (resto == 0 || resto == 1 && digito == 0)
            {
                return true;
            }
            else if (resto >= 2 && resto <= 10)
            {
                if ((11 - resto) == digito)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
