using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Entities
{
    public class Venda : DadosVendedor
    {

        [JsonPropertyOrder(1)]
        [Required]
        public string Item { get; set; }

        [JsonPropertyOrder(2)]
        public EnumStatus Status { get; set; }
    }
}