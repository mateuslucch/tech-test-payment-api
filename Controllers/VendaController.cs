using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using static tech_test_payment_api.Models.ValidacaoCpf;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVenda(Venda venda)
        {

            var vendaBanco = venda;
            if (!TesteCpf(vendaBanco.Cpf))
                throw new Exception("CPF inválido!");

            if (!venda.Status.Equals(EnumStatus.Aguardando_Pagamento))
                throw new Exception("Status inválido! Nova venda deve ter status \"Aguardando Pagamento\"");

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarId), new { id = venda.Id }, venda);
        }

        [HttpGet("BuscaPorId")]
        public IActionResult BuscarId(int id)
        {
            var vendedor = _context.Vendas.Find(id);

            if (vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.Status.Equals(EnumStatus.Entregue))
                return Ok("Produto já entregue...não é possível alterar os dados.");


            if (vendaBanco.Status.Equals(EnumStatus.Cancelada))
                return Ok("Venda foi cancelada...não é possível alterar os dados.");

            if (!TesteCpf(venda.Cpf))
                throw new Exception("CPF inválido!!");

            vendaBanco.Cpf = venda.Cpf;
            vendaBanco.Nome = venda.Nome;
            vendaBanco.Telefone = venda.Telefone;
            vendaBanco.Email = venda.Email;
            vendaBanco.Item = venda.Item;

            if (ChecarStatus(vendaBanco, venda))
            {
                vendaBanco.Status = venda.Status;
            }
            else { throw new Exception($"Status invalido! O status {vendaBanco.Status} não pode ser substituida por {venda.Status}..."); }

            _context.SaveChanges();

            return Ok(venda);
        }

        private bool ChecarStatus(Venda vendaBanco, Venda venda)
        {
            bool novoStatus = false;

            // Atualizar dados sem atualizar status
            if (vendaBanco.Status == venda.Status)
                return true;

            switch (vendaBanco.Status)
            {
                // Atualizar de Aguardando_Pagamento para Cancelada ou Pagamento aprovado
                case EnumStatus.Aguardando_Pagamento:
                    if (venda.Status.Equals(EnumStatus.Pagamento_Aprovado) || venda.Status.Equals(EnumStatus.Cancelada))
                        novoStatus = true;
                    break;
                // Atualizar de Pagamento Aprovado para Enviado_Para_Transportadora ou Cancelada
                case EnumStatus.Pagamento_Aprovado:
                    if (venda.Status.Equals(EnumStatus.Enviado_Para_Transportadora) || venda.Status.Equals(EnumStatus.Cancelada))
                        novoStatus = true;
                    break;
                // Atualizar de Enviado para Transportadora para Entregue
                case EnumStatus.Enviado_Para_Transportadora:
                    if (venda.Status.Equals(EnumStatus.Entregue))
                        novoStatus = true;
                    break;
            }

            return novoStatus;

        }
    }
}