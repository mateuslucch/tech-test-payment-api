## Desafio Tech Pottencial: Payment API

## Projeto de uma API REST de cadastro de vendas em ASP.NET
- Projeto In Memory
- Operações de POST, GET (por id) e PUT (por id)
- Validação com Data Annotations
- Validação de CPF

### - Dados de venda:
- CPF
- Nome
- Telefone
- Email
- Produtos
- Status 

### Possíveis transições na alteração de status:
- Aguardando_Pagamento --> Pagamento_Aprovado
- Aguardando_Pagamento --> Cancelada
- Pagamento_Aprovado --> Enviado_para_Transportadora
- Pagamento_Aprovado --> Cancelada
- Enviado_para_Transportador --> Entregue

## Obs: 
### - Quando em status Cancelado ou Entregue, a API não permite nenhuma modificação nos dados!
### - Nova venda só aceita status Aguardando_Pagamento

<br>

## Contato:
### - [GitHub](https://github.com/mateuslucch)
### - [Linkedin](https://www.linkedin.com/in/mateus-antonio-lucchese/)

<br>

## Exemplo de uso:
## POST
<img src="./Gifs/Teste_POST_e_GET.gif" width="" height="" />

## PUT - 1
<img src="./Gifs/Teste_PUT1.gif" width="" height="" />

## PUT - 2
<img src="./Gifs/Teste_PUT2.gif" width="" height="" />


